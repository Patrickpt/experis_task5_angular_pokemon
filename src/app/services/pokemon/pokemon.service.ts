import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private http: HttpClient) {}

  private url: string = environment.url;

  getAllPokemon(): Promise<any> {
    return this.http.get(`${this.url}/pokemon?limit=1000`).toPromise();
  }

  getPokemonByOffset(limit: number, offset: number): Promise<any> {
    return this.http
      .get(`${this.url}/pokemon?limit=${limit}&offset=${offset}`)
      .toPromise();
  }

  getPokemonByName(name: string): Promise<any> {
    return this.http.get(`${this.url}/pokemon/${name}`).toPromise();
  }
}
