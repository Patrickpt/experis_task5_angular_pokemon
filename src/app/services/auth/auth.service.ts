import { Injectable } from '@angular/core';
import { getStorage } from '../../utils/storage';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  isAuthenticated(): boolean {
    if (!getStorage('trainerName')) {
      return false;
    }
    return true;
  }
}
