import { Component, OnInit } from '@angular/core';
import { setStorage } from '../../../utils/storage';
import { AuthService } from '../../../services/auth/auth.service';
import { RouterLink, Router } from '@angular/router';
import { PokemonService } from '../../../services/pokemon/pokemon.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  trainerName = '';

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    //remove line below, fetches pokemon on page load
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['dashboard']);
    }
  }

  async onLoginClicked() {
    //Route to dashboard, and then to catalogue where the data is fetched
    setStorage('trainerName', this.trainerName);
  }
}
