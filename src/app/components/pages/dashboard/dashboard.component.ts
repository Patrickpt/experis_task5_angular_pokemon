import { Component, OnInit } from '@angular/core';
import { getStorage } from '../../../utils/storage';
import { PokemonService } from '../../../services/pokemon/pokemon.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  storedPokemon: string[];
  pokemonData: Object[] = [];
  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    if (getStorage('storedPokemon')) {
      this.storedPokemon = getStorage('storedPokemon').split(',');
      this.getStoredPokemon();
    }
  }

  async getStoredPokemon() {
    for (let i = 0; i < this.storedPokemon.length; i++) {
      try {
        const res = await this.pokemonService.getPokemonByName(
          this.storedPokemon[i]
        );
        this.pokemonData.push({
          name: this.storedPokemon[i],
          image: res.sprites.front_default,
        });
      } catch (e) {
        console.error(e);
      }
    }
  }
}
