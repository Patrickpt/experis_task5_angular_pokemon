import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from '../../../services/pokemon/pokemon.service';
import { setStorage, getStorage } from '../../../utils/storage';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css'],
})
export class PokemonDetailComponent implements OnInit {
  pokemonName: string = '';
  pokemonData: any;
  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ) {
    this.pokemonName = this.route.snapshot.paramMap.get('pokemonname');
  }

  ngOnInit(): void {
    this.retrievePokemon();
  }

  async retrievePokemon() {
    const res = await this.pokemonService.getPokemonByName(this.pokemonName);
    this.pokemonData = res;
  }

  addPokemonToCollection(): void {
    let storedPokemon: any = '';
    if (getStorage('storedPokemon')) {
      storedPokemon = getStorage('storedPokemon');
    }
    storedPokemon = storedPokemon.toString();
    storedPokemon = storedPokemon ? storedPokemon.split(',') : [];
    if (!storedPokemon.includes(this.pokemonName)) {
      storedPokemon.unshift(this.pokemonName);
      setStorage('storedPokemon', storedPokemon.toString());
      alert(`${this.pokemonName} has been added to your collection!`);
    } else {
      alert(`${this.pokemonName} is already in your collection!`);
    }
  }
}
