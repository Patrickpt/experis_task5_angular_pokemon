import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../../services/pokemon/pokemon.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css'],
})
export class CatalogueComponent implements OnInit {
  constructor(private pokemonService: PokemonService) {}
  limit: number = 10;
  numberOfPokemon: number = 1050;
  currentOffset: number = 0;
  pokemonList: any = [];
  images: string[] = [];
  pokemonImageList: any = [];
  loading: boolean = true;

  ngOnInit(): void {
    this.retrieveNextPokemon(0);
  }

  //Retrieves 10 pokemon, offsetChange decides which pokemon are fetched, relative to the last fetch
  async retrieveNextPokemon(offsetChange: number) {
    //Only execute if there are more pokemon to show
    if (
      this.currentOffset + offsetChange >= 0 &&
      this.currentOffset + offsetChange <= this.numberOfPokemon
    ) {
      try {
        this.pokemonImageList = [];
        this.loading = true;
        this.currentOffset += offsetChange;
        const res: any = await this.pokemonService.getPokemonByOffset(
          this.limit,
          this.currentOffset
        );
        this.pokemonList = res.results;
        this.images = [];
        for (let i = 0; i < this.pokemonList.length; i++) {
          const res: any = await this.pokemonService.getPokemonByName(
            this.pokemonList[i].name
          );
          this.images.push(res.sprites.front_default);
        }
      } catch (e) {
        console.error(e);
      } finally {
        for (let i = 0; i < this.pokemonList.length; i++) {
          this.pokemonImageList.push({
            name: this.pokemonList[i].name,
            image: this.images[i],
          });
        }
        this.loading = false;
      }
    }
  }
}
