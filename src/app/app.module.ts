import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { PokemonDetailComponent } from './components/pages/pokemon-detail/pokemon-detail.component';
import { LoginComponent } from './components/pages/login/login.component';
import { PokemonCardComponent } from './components/components/pokemon-card/pokemon-card.component';
import { HeaderComponent } from './components/components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    CatalogueComponent,
    PokemonDetailComponent,
    PokemonCardComponent,
    HeaderComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
