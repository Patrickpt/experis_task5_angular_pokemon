import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { PokemonDetailComponent } from './components/pages/pokemon-detail/pokemon-detail.component';
import { LoginComponent } from './components/pages/login/login.component';
import { AuthGuard } from '../app/guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'pokemon/:pokemonname',
    component: PokemonDetailComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
